<?php

namespace core\Session;

class Session
{

    public static function token()
    {
        $token = bin2hex(mhash(MHASH_TIGER160, random_bytes(18)));
        $_SESSION['_token'] = $token;
        return $token;
    }

    public static function verifyToken()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") { // the http request method must be post
            if (!isset($_POST['_token'])) { // the _token not passed
                http_response_code(400);
                echo "<h2 style='color:red; text-align:center;'>" . "Invalid csrf Token!" . "<h2>";
                die();
            } else {
                if ($_SESSION['_token'] !== $_POST['_token']) {
                    http_response_code(400);
                    echo "<h2 style='color:red; text-align:center;'>" . "Invalid csrf Token!" . "<h2>";
                    die();
                }
            }
        }
    }

    /* Here the function to set session data */
    public static function set($key, $value)
    {
        if ($key !== '' & $value !== '') {
            $_SESSION[$key] = $value;
        } else {
            return false;
        }
    }

    /* function to check if data existe in the session */
    public static function has($key)
    {
        if (@$_SESSION[$key]) {
            return true;
        } else {
            return false;
        }
    }

    function unset($key) {
        if (self::has($key)) {
            session_unset($key);
        } else {
            return false;
        }
    }

    public static function getAll()
    {
        print_r($_SESSION);
    }

    public function get($key)
    {
        if (self::has($key)) {
            return $_SESSION[$key];
        } else {
            return false;
        }
    }

}
