<?php

use core\Router\Router as App;

App::route("/home", function ($baseUrl) {
    App::show($baseUrl, false);
}, false);
App::route("/posts", function ($baseUrl) {
    App::controller($baseUrl, false);
}, false);
