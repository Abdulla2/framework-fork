<?php

namespace core\Database;

use Exception;
use PDO;

class Database
{

    public $con = '';
    public $host = "localhost";
    public $db = "ura";
    public $user = "root";
    public $pass = "root";
    public $path = "tmp/";

    public function __construct()
    {
        $con = new PDO("mysql:host=$this->host;dbname=$this->db", "$this->user", "$this->pass");
        return $this->con = $con;
    }

    public function get($table)
    {
        return new class($table, $this->con) extends Database
        {

            public $sql; //store the sql query
            public $con; //store the connection
            public $table; //store table name
            public $ops = array('=', '<', '>', '>=', '<='); //query operator

            public function __construct($table, $con)
            {
                $sql = "SELECT * FROM {$table}";
                $this->sql = $sql; //store the sql
                $this->con = $con; //store the connection
                $this->table = $table; //store the table name
            }

            public function all()
            {
                $query = $this->con->prepare($this->sql);
                try {
                    if ($query->execute()) {
                        $query->execute();
                        while ($data = $query->fetchAll(PDO::FETCH_OBJ)) {
                            $allData = $data;
                        }
                        $allData = $allData;
                        return $allData;
                    } else {
                        throw new Exception("Error at line 44");
                    }
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }

            public function where($first, $op, $last)
            {
                if (in_array($op, $this->ops)) {

                    $this->sql .= " WHERE $first $op $last";
                    $query = $this->con->prepare($this->sql);

                    try {
                        if ($query->execute()) {
                            //must be true to execute the qurey or you will get error
                            $tmpname = "$first" . "_" . "$op" . "_" . "$last";
                            while ($data = $query->fetchAll(PDO::FETCH_OBJ)) {
                                $allData = $data;
                            }
                            return $allData;

                        } else {
                            throw new Exception("Error At Line 52 can't execute the query");

                        }
                    } catch (Exception $e) {
                        die($e->getMessage());
                    }

                } else {
                    die("Undifined {$op}");
                }
            }

        };
    }

    public function save($table, $cols, $values)
    {
        $sql = "INSERT INTO $table(";
        foreach ($cols as $col) {
            $sql .= "$col,";
        }

        $sql = rtrim($sql, ',');
        $sql .= ") VALUES (";

        foreach ($values as $value) {
            $sql .= "?,";
        }

        $sql = rtrim($sql, ',');
        $sql .= ")";
        try {
            $prepare = $this->con->prepare($sql);
            if ($prepare->execute($values) === true) {
                return true;
            } else {
                throw new Exception("Faild to insert the data at line 108");
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }

    }

    public function __destruct()
    {
        $this->con = null;
    }

}
