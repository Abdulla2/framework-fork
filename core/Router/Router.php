<?php
/**
 * @package core\Router\main
 */
namespace core\Router;

class Router
{
    /**
     * Holds the value of the url of the page.
     * @var string
     */
    protected static $baseUrl;
    /**
     * Holds data wih comes from uri and used as a paramater for the controller
     * @var array
     */
    protected static $data;
    /**
     * Returns the value of the protected static variable $data
     * @return array the value of protected variable
     */
    public static function getData()
    {
        return self::$data;
    }
    /**
     * Sets the value of the protected static variable $baseurl
     * @param  string $baseUrl the value to set
     * @return null
     */
    public static function url($baseUrl)
    {
        self::$baseUrl = $baseUrl;
    }
    /**
     * Shows a view
     * @param  string $page the name of the controller that will be used.
     * @param  array $data [description]
     * @return null
     */
    public static function show($page, $data)
    {
        if (file_exists("views/" . $page . ".php")) {
            if ($data) {
                self::$data = $data;
            } else {
                self::$data = false;
            }
            require "views/" . $page . ".php";
        } else {
            echo "No view found $page";
        }
    }
    /**
     * Requires a controller
     * @author Abdulla2 abdullaseif2000@gmail.com
     * @license https://opensource.org/licenses/MIT MIT
     * @param  string $page the name of the controller that will be used.
     * @param  array $data [description]
     * @return null
     */
    public static function controller($page, $data)
    {

        if (file_exists("controllers/" . $page . ".php")) {
            if ($params) {
                self::$data = $data;
            } else {
                self::$data = false;
            }
            require "controllers/" . $page . ".php";
            $controller = new App\controllers\$page;
            if ((int) method_exists($controller, "index")) {
                ($data == true) ? call_user_func_array(array($controller, "index"), $params) : call_user_func(array($controller, "index"));
            } elseif ((int) method_exists($controller, $page)) {
                ($data == true) ? call_user_func_array(array($controller, $page), $params) : call_user_func(array($controller, $page));
            } else {
                echo "no method found for the controller $page";
            }
        } else {
            echo "No controller found $page";
        }
    }

    public static function requestMethod($type)
    {
        switch ($type) {
            case 'POST':
                if ($_SERVER['REQUEST_METHOD'] !== $type) {
                    require 'views/errors/POST.php';
                    exit();
                }
                break;
            case 'GET':
                if ($_SERVER['REQUEST_METHOD'] !== $type) {
                    require 'views/errors/GET.php';
                    exit();
                }
                break;
        }
    }
    /**
     * The main routing method that is used  to route a uri to do a specific function
     * @param  string $urls        the uri for the routing
     * @param  function $call        the function that will be called for the specified uri
     * @param  string $requestType get or post
     * @return null
     */
    public static function route($urls, $call, $requestType)
    {
        $urls = explode("/", $urls);
        $baseUrls = $urls[1];
        unset($urls[1]);
        unset($urls[0]);
        $baseUrls = explode(" ", $baseUrls);
        $baseUrls_params = $urls;

        $htaccess_urls = self::$baseUrl;
        $htaccess_urls = explode("/", $htaccess_urls);
        $htaccess_base_url = $htaccess_urls[0];
        unset($htaccess_urls[0]);
        $htaccess_params = $htaccess_urls;

        if (in_array($htaccess_base_url, $baseUrls)) {
            foreach ($baseUrls as $baseUrl) {
                self::requestMethod($requestType);
                if (sizeof($baseUrls_params) >= 1 and sizeof($htaccess_params) >= 1) {
                    @$params = array_combine($baseUrls_params, $htaccess_params);
                    if ($params) {
                        if (is_callable($call)) {
                            call_user_func($call, $baseUrl, $params);
                        }
                    } else {
                        echo "Parameters doesn't Match .";
                    }
                } elseif (sizeof($baseUrls_params) < 1 and sizeof($htaccess_params) < 1) {
                    if (is_callable($call)) {
                        call_user_func($call, $baseUrl);
                    }
                } else {
                    echo "Parameters doesn't Match .";
                }
            }
        }
    }
}
