<?php

session_start();
session_regenerate_id(true);
require 'vendor/autoload.php';
use core\Router\Router as App;

App::url($_GET['url']);
require 'core/Routes/Routes.php';
